angular.module("app", ['ui.bootstrap'])
	.controller("ctrl", function($scope, $http, $filter) {
		$scope.changeTime = "1h";
		$scope.priceCoin = 'usd';
		$scope.rank = true;
		$scope.sortObject = {};

		$scope.pageChanged = function() {
			$scope.updateList();
		};
		$scope.sortingBy = function(sortPr) {
			if($scope.sortObject[sortPr] == undefined || $scope.sortObject == null) {
				$scope.sortObject[sortPr] = true;
			}else {
				$scope.sortObject[sortPr] = !$scope.sortObject[sortPr];
			}
			
			$scope.rates = $filter('orderBy')($scope.rates, function(value){
				value = value[sortPr].replace('$', '');
				if(parseFloat(value)) {
					value = parseFloat(value);
				}
				return value;
			}, !$scope.sortObject[sortPr]);
		}
		$scope.persentColor = function (persentValue) {
			return persentValue < 0 ? 'text-error': 'text-success';
		}
		$scope.changeRank = function() {
			$scope.rank = !$scope.rank;
			$scope.updateList();
		}
		$scope.updateList = function() {
			var startPage = $scope.currentPage == 1 ? $scope.currentPage - 1 : $scope.currentPage;
			var rates = $scope.request.slice(startPage, $scope.currentPage + $scope.itemsPerPage - 1);
			$scope.rates = $scope.rank ? rates : rates.reverse();
		}
		$http.get("https://api.coinmarketcap.com/v1/ticker/", {cache : true})
		 	.then(function(request){
		 		$scope.request = request.data;
				$scope.currentPage = 1;
				$scope.itemsPerPage = 30;
				$scope.rates = $scope.request.slice(0,$scope.itemsPerPage);
				$scope.totalItems = $scope.request.length;
			});
	})
	.filter('paginated', function() {
		return function(input, currentPage, itsPerPage) {
			var startPage = currentPage == 1 ? currentPage - 1 : currentPage;
			return input.slice(startPage, currentPage + itsPerPage - 1);
		}
	});




